#include <16F73.h>
#device *=16

#FUSES WDT                      //Watch Dog Timer
#FUSES PUT                      //Power Up Timer
#FUSES BROWNOUT               //brownout reset
#FUSES HS

#use delay(crystal=20000000)
#use rtos(timer=0, minor_cycle=1 ms)
#USE PWM(OUTPUT=PIN_C1, TIMER=2, FREQUENCY=20kHz, PERIOD=0, BITS=10, PWM_OFF, STREAM=light)
#USE PWM(OUTPUT=PIN_C2, TIMER=2, FREQUENCY=20kHz, PERIOD=0, BITS=10, PWM_OFF, STREAM=bl)

#define Logic_ch2  PIN_B6
#define Logic_ch1  PIN_B7

#define KEY1  PIN_A0
#define KEY2  PIN_A1
#define KEY3  PIN_A2
#define KEY4  PIN_A3

#define LCD_ENABLE_PIN PIN_B4
#define LCD_RS_PIN PIN_B6
#define LCD_RW_PIN PIN_B5
#define LCD_DATA4 PIN_B3
#define LCD_DATA5 PIN_B2
#define LCD_DATA6 PIN_B1
#define LCD_DATA7 PIN_B0

#define Key_switch_time 5
