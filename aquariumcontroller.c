#include <aquariumcontroller.h>
#include "flex_lcdm.c"
#include "ds1307.c"

struct TTime{
       int8 hrs;
       int8 min;
       int8 sec;
     };  

//TASK MENU VARIABLES
int8 mode = 0;
int8 mode_old = !mode;
int8 lightMode = 0;
unsigned int16 displayLightTime = 0;
unsigned int16 displayLightlevel = 0;

//TASK TIME VARIABLES
struct TTime CurrentTime; 
struct TTime Sunrise;
struct TTime Sunset;
int16 RiseTime = 1;

//TASK KEY 1 VARIABLES
unsigned int8 Key1_count = 0;
int1 Key1_oldstate = input_state(KEY1);

//TASK KEY 2 VARIABLES
unsigned int8 Key2_count = 0;
int1 Key2_oldstate = input_state(KEY2);

//TASK KEY 3 VARIABLES
unsigned int8 Key3_count = 0;
int1 Key3_oldstate = input_state(KEY3);

//TASK KEY 4 VARIABLES
unsigned int8 Key4_count = 0;
int1 Key4_oldstate = input_state(KEY4);

//TASK PWM VARIABLES
unsigned int16 pwmLevel = 0;
unsigned int16 pwmCount = 0;
unsigned int8 percentLevel = 0;

//Advertisements
void initialization();
void loadSettings();
void saveTime();
void displayLightActivate();
int16 timeToInt(int8 hrs, int8 min);


//TASK DISPLAY PROCESSED
#task(rate = 100 ms, max = 125 us)
void task_display_out_processed();

//TASK READ TIME PROCESSED
#task(rate = 100 ms, max = 125 us)
void task_read_time_processed();

//TASK KEY1 PROCESSED
#task(rate = 10 ms, max = 125 us)
void task_key_1_processed();

//TASK KEY2 PROCESSED
#task(rate = 10 ms, max = 125 us)
void task_key_2_processed();

//TASK KEY3 PROCESSED
#task(rate = 10 ms, max = 125 us)
void task_key_3_processed();

//TASK KEY4 PROCESSED
#task(rate = 10 ms, max = 125 us)
void task_key_4_processed();

//TASK PWM  PROCESSED
#task(rate = 16 ms, max = 156 us)
void task_pwm_processed();

//FUNCTION IMPLEMENTATION
void initialization() {
   SET_TRIS_A( 0x3F );
   SET_TRIS_B( 0x00 );
   SET_TRIS_C( 0x00 );

   setup_wdt(WDT_2304MS);
   restart_wdt();

   pwm_set_duty(light, 0);
   pwm_on(light);
   
   pwm_set_duty(bl, 0);
   pwm_on(bl);   
   
   ds1307_init();
   lcd_init();
   printf(lcd_putc,"\f");
   loadSettings();
}

void loadSettings() {
   Sunrise.hrs = ds1307_get_data(0);
   if (Sunrise.hrs > 23) Sunrise.hrs = 23;
   Sunrise.min = ds1307_get_data(1);
   if (Sunrise.min > 59) Sunrise.min = 59;
   Sunset.hrs = ds1307_get_data(2);
   if (Sunset.hrs > 23) Sunset.hrs = 23;
   Sunset.min = ds1307_get_data(3);
   if (Sunset.min > 59) Sunset.min = 59;
   RiseTime = ds1307_get_data(4);
   if (RiseTime > 90) RiseTime = 90;
   if (RiseTime < 1) RiseTime = 90;
   lightMode = ds1307_get_data(5);
   if (lightMode > 2) lightMode = 0;
}

void saveTime() {
   ds1307_set_time(CurrentTime.hrs, CurrentTime.min, CurrentTime.sec);
}

void displayLightActivate() {
   displayLightTime = 100;
   displayLightlevel = 31;
}

int16 timeToInt(int8 hrs, int8 min) {
   int16 h = hrs;
   int16 ret = h << 5;
   ret += h << 4;
   ret += h << 3;
   ret += h << 2;
   ret += min;
   return ret;
}

//TASK DISPLAY OUT PROCESSED
void task_display_out_processed(){
   if (mode_old != mode) {
      mode_old = mode;
      printf(lcd_putc,"\f");
   }
   switch (mode) {
      case 0: 
         switch (lightMode) {
            case 0:
               printf(lcd_putc,"\r <> %02u:%02u:%02u <>\nA��o�a�     %03u%%", 
               CurrentTime.hrs, 
               CurrentTime.min, 
               CurrentTime.sec, 
               percentLevel);
               break;  
            case 1:
               printf(lcd_putc,"\r <> %02u:%02u:%02u <>\nB����e�o    %03u%%", 
               CurrentTime.hrs, 
               CurrentTime.min, 
               CurrentTime.sec, 
               percentLevel);
               break; 
            case 2:
               printf(lcd_putc,"\r <> %02u:%02u:%02u <>\nBú���e�o   %03u%%", 
               CurrentTime.hrs, 
               CurrentTime.min, 
               CurrentTime.sec, 
               percentLevel);
               break;                
         }
         break;   
      case 1: 
         printf(lcd_putc,"\rHac�po��a �aco�\n %02u:%02u:%02u", CurrentTime.hrs, CurrentTime.min, CurrentTime.sec);
         break;
      case 2: 
         printf(lcd_putc,"\rBpe�� �ocxo�a \n %02u:%02u", Sunrise.hrs, Sunrise.min);
         break;
      case 3: 
         printf(lcd_putc,"\rBpe�� �a�a�a \n %02u:%02u", Sunset.hrs, Sunset.min);
         break;
      case 4: 
         printf(lcd_putc,"\rBpe�� �����e��� \n %02Lu ���y� ", RiseTime);
         break;   
   }
   
   if (displayLightTime > 0) displayLightTime --;
   if (displayLightTime == 0) mode = 0;
   if (displayLightTime == 0 && displayLightlevel > 0) displayLightlevel --;
   pwm_set_duty(bl, displayLightlevel * displayLightlevel);
}

//TASK READ TIME PROCESSED
void task_read_time_processed() {
   ds1307_get_time(CurrentTime.hrs, CurrentTime.min, CurrentTime.sec);
}

//TASK KEY1 PROCESSED
void task_key_1_processed() {
restart_wdt();
   if ( Key1_count > 0 && input_state(KEY1) != Key1_oldstate) Key1_count --; 
      else Key1_count = Key_switch_time;  
   if (Key1_count != 0) return;
   Key1_oldstate = input_state(KEY1);
   Key1_count = Key_switch_time;
   if (!input_state(KEY1)) {
       displayLightActivate();
       mode ++;
       if (mode > 4) mode = 0;
   }
}

//TASK KEY2 PROCESSED
void task_key_2_processed() {
   if ( Key2_count > 0 && input_state(KEY2) != Key2_oldstate) Key2_count --; 
      else Key2_count = Key_switch_time;  
   if (Key2_count != 0) return;
   Key2_oldstate = input_state(KEY2);
   Key2_count = Key_switch_time;
   if (!input_state(KEY2)) {
      displayLightActivate();
      switch (mode) {
         case 0:
            lightMode = 0;
            mode_old = 10;
            ds1307_set_data(5, lightMode);
            break;
         case 1: 
            CurrentTime.hrs ++;
            if (CurrentTime.hrs > 23) CurrentTime.hrs = 0;
            saveTime();
            break;
         case 2: 
            Sunrise.hrs ++;
            if (Sunrise.hrs > 23) Sunrise.hrs = 0;
            ds1307_set_data(0, Sunrise.hrs);
            break;   
         case 3: 
            Sunset.hrs ++;
            if (Sunset.hrs > 23) Sunset.hrs = 0;
            ds1307_set_data(2, Sunset.hrs);
            break;              
         case 4: 
            RiseTime ++;
            if (RiseTime > 90) RiseTime = 1;
            ds1307_set_data(4, RiseTime);
            break; 
      }
   }
}
//TASK KEY3 PROCESSED
void task_key_3_processed() {
   if ( Key3_count > 0 && input_state(KEY3) != Key3_oldstate) Key3_count --; 
      else Key3_count = Key_switch_time;  
   if (Key3_count != 0) return;
   Key3_oldstate = input_state(KEY3);
   Key3_count = Key_switch_time;
   if (!input_state(KEY3)) {
      displayLightActivate();
      switch (mode) {
         case 0:
            lightMode = 1;
            mode_old = 10;
            ds1307_set_data(5, lightMode);
            break;
         case 1: 
            CurrentTime.min ++;
            if (CurrentTime.min > 59) CurrentTime.min = 0;
            saveTime();
            break;
         case 2: 
            Sunrise.min ++;
            if (Sunrise.min > 59) Sunrise.min = 0;
            ds1307_set_data(1, Sunrise.min);
            break;
         case 3: 
            Sunset.min ++;
            if (Sunset.min > 59) Sunset.min = 0;
            ds1307_set_data(3, Sunset.min);
            break;
         case 4: 
            if (RiseTime == 1) RiseTime = 90;
            else RiseTime --;
            ds1307_set_data(4, RiseTime);
            break;             
      }
   }
}
//TASK KEY4 PROCESSED
void task_key_4_processed() {
   if ( Key4_count > 0 && input_state(KEY4) != Key4_oldstate) Key4_count --; 
      else Key4_count = Key_switch_time;  
   if (Key4_count != 0) return;
   Key4_oldstate = input_state(KEY4);
   Key4_count = Key_switch_time;
   if (!input_state(KEY4)) {
      displayLightActivate();
      switch (mode) {
         case 0:
            lightMode = 2;
            mode_old = 10;
            ds1307_set_data(5, lightMode);
            break;
         case 1: 
            CurrentTime.sec = 0;
            saveTime();
            break;
      }
   }
}

//TASK PWM  PROCESSED
void task_pwm_processed() {
   if (pwmCount > 0) pwmCount --;  
   if (pwmCount != 0) return;
   pwmCount = RiseTime * (7 - (pwmLevel >> 7));
   int1 aon = 0;
   int1 vs = 0;

   int16 time = timeToInt(CurrentTime.hrs, CurrentTime.min);
   if ((time > timeToInt(Sunrise.hrs, Sunrise.min)) && (time < timeToInt(Sunset.hrs, Sunset.min))) aon = 1;
   if (lightMode == 0) vs = aon;
   if (lightMode == 1) vs = 1;
   if (lightMode == 2) vs = 0;
   
   if (vs && pwmLevel < 1000) pwmLevel ++;
   if (!vs && pwmLevel > 0) pwmLevel --;
   percentLevel = pwmLevel / 10;
   pwm_set_duty(light, pwmLevel);
}

void main()
{
   initialization();
   rtos_run();
}
